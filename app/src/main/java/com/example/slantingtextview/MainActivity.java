package com.example.slantingtextview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity {
    private SlantedTextView slantingTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        slantingTextView=findViewById(R.id.slv_right);
        slantingTextView.setSlantedBackgroundColor(Color.parseColor("#3700B3"));
        slantingTextView.setText("剩余28天");
    }
}